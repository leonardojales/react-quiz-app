import express from 'express'
import path from 'path'
import fallback from 'express-history-api-fallback'
import devConfig from './config/dataConfig'
import { NODE_ENV, PORT } from './config/dataConfig'
import mongoose from 'mongoose'
import bodyParser from 'body-parser'

const dbUrl = 'mongodb://localhost:27017/quiz';

import apiRoutes from './routes/api-routes'

// Link app to express module
const app = express();

// Load Configs
devConfig(app);

// BodyParser to JSON
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// test connection to database
mongoose.Promise = global.Promise;
mongoose.connect(dbUrl, () => { console.log('connected through mongoose') });

// connect api routes
app.use(apiRoutes);

app.use(fallback(path.join(__dirname, '../client/index.html')));

app.listen(PORT, (err) => {
  if (err) throw err;
  console.log(`The Express Server is Listening at port ${PORT} in ${NODE_ENV} mode`);
});

export default app;