import axios from 'axios'

import { ADD_QUIZ, SAVE_QUIZ } from '../static/quiz'

// Components Actions using axios library

// AddQuizz locally for save in Database
export function addQuizLocal(quiz){
	return {
		type: ADD_QUIZ,
		quiz
	}
}

//Dispatch Save Function
export function saveNewQuiz(data) {
	return dispatch => {
		axios.post('/save', data).then( (response) => {
			dispatch(addQuizLocal(data.quiz));
		}).catch(err => console.log(err));
	}
}

// SaveQuiz extracted from getQuiz
export function saveQuiz(data){
	return {
		type: SAVE_QUIZ,
		data
	}
}

// Dispatch get function
export function getQuiz() {
	return dispatch => {
		axios.get('/get').then( (response) => {
			const data = response.data;
			dispatch(saveQuiz(data));
		});
	}
}

// Not used
export function updateQuiz(data) {
	return dispatch => {
		axios.post('/update', data).then( (response) => {
			console.log(response);
		}).catch(err => console.log(err));
	}
}