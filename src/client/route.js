import React from 'react'
import { Route, IndexRoute } from 'react-router'

import App from './containers/app'
import Home from './components/home'
import Quiz from './containers/quiz'
import NewQ from './containers/newQuiz'


// Define Routes
export default (
  <Route name = 'home' component = {App}>
  	<Route path = '/' name = 'home' component = {Home} />
  	<Route path = 'quiz' name = 'quiz' component = {Quiz} />
  	<Route path = 'newQuiz' name = 'newQuiz' component = {NewQ} />
  </Route>
);
