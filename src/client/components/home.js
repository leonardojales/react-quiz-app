import React from 'react'
import { connect } from 'react-redux'
import { browserHistory, Link } from 'react-router'

class home extends React.Component {
	// Render Main Home Component
 	render() {
 		return (
		  <div className = 'homeComp'>
		    	<h1><b>Welcome to Quiz Technical Challenge</b></h1>
				<div><h2>{'eJOY'}</h2></div>
				<h3>Create Before Start</h3>
		  </div>
	  );
 	}
}
export default home;
