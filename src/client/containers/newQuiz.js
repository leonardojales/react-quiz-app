import React from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { PropTypes } from 'react';

import { saveNewQuiz, getQuiz } from '../actions/quiz'

// Connect to database load getQuiz and saveNewQuiz to dispatch
@connect(
	null,
	dispatch => ({
		getQuiz: bindActionCreators(getQuiz, dispatch),
		saveNewQuiz: bindActionCreators(saveNewQuiz, dispatch)
	})
)
export default class NewQuiz extends React.Component {
	//Default Structure for make new Quizzes/Questions/Answers
	constructor() {
		super();
		this.state = {
			quiz: {
				title: '',
				questions: []
			},
			review: false,
			reviewRequest: false,
			editQuizTitleState: false,
			editQuizTitle: '',
			editReview: new Map(),
			title: false,
			quizTitle: '',
			questionTitle: '',
			answers: [''],
			correctAnswer: null,
			submission: false
		}
		this.handleKeyPress = this.handleKeyPress.bind(this);
		this.setQuizTitle = this.setQuizTitle.bind(this);
		this.handleInput = this.handleInput.bind(this);
		this.addAnswerOption = this.addAnswerOption.bind(this);
		this.removeOption = this.removeOption.bind(this);
		this.submitAndContinue = this.submitAndContinue.bind(this);
		this.submitAndReview = this.submitAndReview.bind(this);
		this.editQuestion = this.editQuestion.bind(this);
		this.handleEditQuizTitle = this.handleEditQuizTitle.bind(this);
		this.submitTitleEdit = this.submitTitleEdit.bind(this);
		this.cancelTitleEdit = this.cancelTitleEdit.bind(this);
		this.submitQuestionEdit = this.submitQuestionEdit.bind(this);
		this.setCorrectAnswerEdit = this.setCorrectAnswerEdit.bind(this);
		this.handleOptionEdit = this.handleOptionEdit.bind(this);
		this.removeOptionEdit = this.removeOptionEdit.bind(this);
		this.addEditOption = this.addEditOption.bind(this);
		this.handleOptionTitleEdit = this.handleOptionTitleEdit.bind(this);
		this.reviewAddQuestion = this.reviewAddQuestion.bind(this);
		this.removeQuestion = this.removeQuestion.bind(this);
		this.saveQuiz = this.saveQuiz.bind(this);
	}
	// Function for handle the add answerOption
	handleKeyPress(key) {
		if (key.keyCode === 9) {
			const { answers } = this.state;
			if (answers[answers.length - 1] !== '') {
				this.addAnswerOption();
			}
		}
	}
	// Function for Set Quiz title to current state
	setQuizTitle() {
		const { quiz, quizTitle } = this.state;
		if (quizTitle !== '') {
			quiz.title = quizTitle;
			this.setState({
				title: true,
				quiz
			});
		}
	}
	// Function for handle inputs events
	handleInput(event) {
		this.setState({
			[event.target.name]: event.target.value
		});
	}
	// Function for handle options events
	handleOption(idx, event) {
		const { answers } = this.state;
		answers[idx] = event.target.value;
		this.setState({ answers });
	}
	// Function for set answer options to answer current state options
	addAnswerOption() {
		const { answers } = this.state;
		answers.push('')
		this.setState({ answers });
	}
	// Function for handle the options remove
	removeOption(idx) {
		const { answers } = this.state;
		if (answers.length > 1) {
			answers.splice(idx, 1);
			this.setState({ answers });
		}
	}
	// Function for set the correct answer to current state
	setCorrectAnswer(idx) {
		this.setState({
			correctAnswer: idx
		});
	}
	// Function for Submit one question and continue editing another one
	submitAndContinue() {
		const { quiz, answers, correctAnswer, questionTitle } = this.state;
		const filteredAnswers = answers.filter( (answer) => answer !== '' );
		if (correctAnswer !== null && questionTitle !== '' && filteredAnswers.length > 3) {
			const question = {
				questionTitle,
				correctAnswer,
				answers: filteredAnswers
			}
			quiz.questions.push(question);
			this.setState({
				quiz,
				questionTitle: '',
				answers: [''],
				correctAnswer: null,
				message: 'Question Added!'
			});
			setTimeout( () => { this.setState({ message: '' }) }, 3000);
		}
	}
	// Function for Submit all questions for review's state
	submitAndReview() {
		const { quiz, answers, correctAnswer, questionTitle } = this.state;
		const filteredAnswers = answers.filter( (answer) => answer !== '' );
		if (correctAnswer !== null && questionTitle !== '' && filteredAnswers.length > 3) {
			const question = {
				questionTitle,
				correctAnswer,
				answers: filteredAnswers
			}
			quiz.questions.push(question);
			this.setState({
				quiz,
				questionTitle: '',
				answers: [''],
				correctAnswer: null,
				review: true
			});
		}
	}
	// Function for handle quizTitle state 
	handleEditQuizTitle() {
		this.setState({
			editQuizTitleState: true,
			editQuizTitle: this.state.quizTitle
		});
	}
	// Function for submit edited title
	submitTitleEdit() {
		if (this.state.editQuizTitle !== '') {
			this.setState({
				editQuizTitleState: false,
				quizTitle: this.state.editQuizTitle,
				editQuizTitle: ''
			});
		}
	}
	// Function for cancel edited title
	cancelTitleEdit() {
		this.setState({
			editQuizTitleState: false,
			editQuizTitle: ''
		});
	}
	// Function for edit question by ID
	editQuestion(idx) {
		const { editReview, quiz } = this.state;
		editReview.set(idx, null);
		const question = quiz.questions[idx];
		const questionID = 'editQuestion' + idx;
		this.setState({
			[questionID]: question,
			editReview,
		});
	}
	// Function for handle edit options title
	handleOptionTitleEdit(questionIdx, event) {
		const editQuestionID = 'editQuestion' + questionIdx;
		const editQuestion = this.state[editQuestionID];
		editQuestion.questionTitle = event.target.value
		this.setState({
			[editQuestionID]: editQuestion
		});
	}
	// Function for set the correct answer edited
	setCorrectAnswerEdit(questionIdx, answerIdx) {
		const editQuestionID = 'editQuestion' + questionIdx;
		const editQuestion = this.state[editQuestionID];
		editQuestion.correctAnswer = answerIdx;
		this.setState({
			[editQuestionID]: editQuestion
		});
	}
	// Function for remove edited option
	removeOptionEdit(questionIdx, answerIdx) {
		const editQuestionID = 'editQuestion' + questionIdx;
		const editQuestion = this.state[editQuestionID];
		if (editQuestion.answers.length > 1) {
			editQuestion.answers.splice(answerIdx, 1);
			if (editQuestion.correctAnswer > answerIdx) {
				editQuestion.correctAnswer -= 1;
			} else if (editQuestion.correctAnswer === answerIdx) {
				editQuestion.correctAnswer = null;
			}
			this.setState({
				[editQuestionID]: editQuestion
			});
		}
	}
	// Function for handle the edited options
	handleOptionEdit(questionIdx, answerIdx, event) {
		const editQuestionID = 'editQuestion' + questionIdx;
		const editQuestion = this.state[editQuestionID];
		editQuestion.answers[answerIdx] = event.target.value;
		this.setState({
			[editQuestionID]: editQuestion
		});
	}
	// Function for add edited questions 
	addEditOption(idx) {
		const editQuestionID = 'editQuestion' + idx;
		const editQuestion = this.state[editQuestionID];
		editQuestion.answers.push('');
		this.setState({
			[editQuestionID]: editQuestion
		});
	}
	// Function for submit edited questions to save
	submitQuestionEdit(idx) {
		const editQuestionID = 'editQuestion' + idx;
		const editedQuestion = this.state[editQuestionID];
		const { quiz, editReview } = this.state;
		if (editedQuestion.correctAnswer !== null && editedQuestion.answers.length > 3) {
			quiz.questions[idx] = editedQuestion;
			editReview.delete(idx);
			this.setState({
				quiz,
				editReview
			});
		}
	}
	// Function for remove question from local
	removeQuestion(idx) {
		const { quiz } = this.state;
		quiz.questions.splice(idx, 1);
		this.setState({ quiz });
	}
	// Function for add questions for add in review state
	reviewAddQuestion() {
		const { quiz, editReview } = this.state;
		quiz.questions.push({
			answers: [''],
			correctAnswer: null,
			questionTitle: ''
		});
		this.setState({ quiz });
		this.editQuestion(quiz.questions.length - 1);
	}
	// Function for saveQuiz in payload and send to action saveNewQuiz
	saveQuiz() {
		const { quiz } = this.state;
		if (quiz.questions.length > 0) {
			const payload = {
				quiz
			}
			this.props.saveNewQuiz(payload);
			this.setState({
				review: false,
				submission: true
			});
		}
	}
	// Render
	render() {
		const { correctAnswer, answers } = this.state;
		const renderAnswers = answers.map( (ans, idx) => {
			const { answers } = this.state;
			return (
				<form className = 'card p-3' key = {idx}>
					<div className = 'input-group'>
						<span className = 'input-group-addon'>
						{ correctAnswer === idx ?
							<i className = "fa fa-check-box fa-check-square-o" aria-hidden = "true">
							</i>
							:
							<i
								id = 'checkBox'
								className = "fa fa-check-box fa-square-o"
								aria-hidden = "true"
								onClick = {this.setCorrectAnswer.bind(this, idx)}>
							</i>
						}
						</span>

						<input
							type = 'text'
							name = {'answer_'+{ans}}
							placeholder = 'Enter an Answer and Press Tab to Add Another' 
							value = {answers[idx]}
							onChange = {this.handleOption.bind(this, idx)}
							className = 'form-control' />

						<span className = 'input-group-addon'>
							<i
								onClick = {this.removeOption.bind(this, idx)}
								className = "fa fa-times"
								aria-hidden = "true">
							</i>
						</span>
					</div>
				</form>
			);
		})
		return (
			<div className = 'container'>
				{ !this.state.title && !this.state.review &&

					<div className = 'col-md-5 form-area'>
						<h1>Create a Quiz</h1>
						<p>You create one new quiz here. Add as many questions as you want, always editing them later! After final submission you cannot edit the quiz.</p>
						<h3>Enter a Title for Your Quiz:</h3>
						<form className = 'card p-2'>
							<div className = 'input-group'>
								<input
								type = 'text'
								name = 'quizTitle'
								placeholder = 'Enter a Title' 
								value = {this.state.quizTitle}
								onChange = {this.handleInput}
								className = 'form-control' />
								<span className = 'input-group-btn'>
									<button
										className = 'btn btn-primary'
										onClick = {this.setQuizTitle}>
										Submit Title and Add Questions
									</button>
								</span>
							</div>
						</form>
					</div>
				}

				{ this.state.title && !this.state.review && !this.state.submission &&

					<div className = 'col-md-5 form-area'>
						<h1>Add Questions for Your Quiz:</h1>
						<p>Be sure to add at least four answers and check the box next to correct one!</p>
						<h3>Question {this.state.quiz.questions.length+1}:</h3>
						<input
							type = 'text'
							name = 'questionTitle'
							placeholder = 'Enter a Question' 
							value = {this.state.questionTitle}
							onChange = {this.handleInput}
							className = 'form-control' />
						<h3>Answers:</h3>
						{renderAnswers}
						<div className = 'btn-group'>
							<button
								className = 'btn btn-primary'
								onClick = {this.addAnswerOption}>
								Add Answer
							</button>
							<button
								className = 'btn btn-primary'
								onClick = {this.submitAndContinue}>
								Add Question
							</button>
							<button
								className = 'btn btn-primary'
								onClick = {this.submitAndReview}>
								Submit Quiz 
							</button>
						</div>
					</div>
				}

				{ !this.state.review && <p className = 'message'>{this.state.message}</p> }

				{ this.state.review &&

					<div className = 'col-md-5 form-area'>

						<h1>Review Your Quiz</h1>
						
						{ !this.state.editQuizTitleState ? 
							<h2 className = 'panel panel-default'>
								<i
									onClick = {this.handleEditQuizTitle}
									className = "fa fa-pencil-square-o fa-editQuestion"
									aria-hidden = "true">
								</i>
								Quiz Title: {this.state.quizTitle}
							</h2>
							:
							<div>
								<h2 className = 'panel panel-default' >Edit Quiz Title:</h2>
								<div className = 'form'>
									<input
										type = 'text'
										name = 'editQuizTitle'
										value = {this.state.editQuizTitle}
										onChange = {this.handleInput}
										className = 'form-control'
									/>
									<div className = 'btn-group'>
										<button onClick = {this.cancelTitleEdit}className = 'btn btn-primary'>Cancel Edit</button>
										<button onClick = {this.submitTitleEdit}className = 'btn btn-primary'>Submit Title Edit</button>
									</div>
								</div>
							</div>
						}

						{	this.state.quiz.questions.map( (question, idx) => {
								const { editReview } = this.state;
								const { correctAnswer } = question;
								if (editReview.has(idx)) {
									const questionID = 'editQuestion' + idx;
									return (
										<div key = {idx}>
											<h2 className = 'panel panel-default'>
												Editing Question {idx + 1}:
											</h2>
											<h3>Edit Question Title:</h3>
											<input
												type = 'text'
												placeholder = 'Enter an Question Title'
												value = {this.state[questionID].questionTitle}
												onChange = {this.handleOptionTitleEdit.bind(this, idx)}
												className = 'form-control'
											/>
											<h3>Edit Question Answers:</h3>
											{
												question.answers.map( (answer, index) => {
													return (
														<div className = 'input-group' key = {index}>
															<span className = 'input-group-addon'>
															{ this.state[questionID].correctAnswer === index ?
																<i
																	className = "fa fa-check-box fa-check-square-o" aria-hidden = "true">
																</i>
																:
																<i
																	id = 'checkBox'
																	className = "fa fa-check-box fa-square-o"
																	aria-hidden = "true"
																	onClick = {this.setCorrectAnswerEdit.bind(this, idx, index)}>
																</i>
															}
															</span>
															<input
																type = 'text'
																value = {this.state[questionID].answers[index]}
																onChange = {this.handleOptionEdit.bind(this, idx, index)}
																placeholder = 'Enter an Answer and Press Tab to Add Another'
																className = 'form-control' />
															<span className = 'input-group-addon'>
																<i
																	onClick = {this.removeOptionEdit.bind(this, idx, index)}
																	className = "fa fa-times"
																	aria-hidden = "true">
																</i>
																</span>
														</div>
													);
												})
											}
											<div className = 'input-group'>
												<button onClick = {this.addEditOption.bind(this, idx)} className = 'btn btn-primary'>Add Answer</button>
												<button onClick = {this.submitQuestionEdit.bind(this, idx)} className = 'btn btn-primary'>Save Edit</button>
											</div>
										</div>
										)
								} else {
									return (
										<div key = {idx}>
											<h3>Question {idx + 1}:</h3>
											<h2 className = 'panel panel-default'>
												<i
													onClick = {this.editQuestion.bind(this, idx)}
													className = "fa fa-pencil-square-o fa-editQuestion"
													aria-hidden = "true">
												</i>
												<i
													onClick = {this.removeQuestion.bind(this, idx)}
													className = "fa fa-trash fa-editQuestion"
													aria-hidden = "true">
												</i>
												{question.questionTitle}
											</h2>
											<h3>Answers:</h3>
											{question.answers.map( (answer, index) => {
												let style = {
													background: 'rgba(225,225,225,0.5)'
												}
												if (correctAnswer === index) {
													style = {
														background: 'rgb(113, 245, 161)'
													}
												}
												return (
													<div key = {index} style = {style}>
														<h4>{answer}</h4>
													</div>
												);
											}) }
										</div>
									);
								}
							})
						}

						{ this.state.quiz.questions.length === 0 && <h2 className = 'errorMsg'>You have to enter some questions to submit a new quiz!</h2> }

						<div className = 'btn-group'>
							<button className = 'btn btn-primary' onClick = {this.reviewAddQuestion}>Add Question</button>
							<button className = 'btn btn-primary' onClick = {this.saveQuiz}>Save and Submit Your Quiz</button>
						</div>

					</div>

				}

				{ this.state.submission &&

					<div className = 'submissionSuccess'>
						<h1>Your quiz was saved successfully!</h1>
						<h2>Go to the <Link to = 'quiz'>Quizs Page</Link> to view it and start solving.</h2>
					</div>

				}
			</div>
		);
	}
	// Load get requisition prevent unload elements and KeyPressHandler Event
	componentDidMount() {
		this.props.getQuiz();
		window.addEventListener('keydown', this.handleKeyPress);
	}
};