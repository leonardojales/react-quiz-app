import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router'

export default class Navbar extends Component {
  // Render Navigation Bar Component
  render() {
    return (
      <div className="ui container">
        <div className="ui three item menu">
          <Link to = '/' className = 'item' activeClassName = 'activeLink'>Home</Link>
          <Link to = 'quiz' className = 'item' activeClassName = 'activeLink'>Quiz</Link>
          <Link to = 'newQuiz' className = 'item' activeClassName = 'activeLink'>Create</Link>
         </div>
      </div>
    );
  }
};