import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { getQuiz } from '../actions/quiz'

import Navbar from '../components/navbar'
import Footer from '../components/footer'

// Connect to database load getQuiz to dispatch
@connect( 
	null,
	dispatch => ({
		getQuiz: bindActionCreators(getQuiz, dispatch)
	})
)
export default class App extends React.Component {
  // Load get requisition prevent unload elements
  componentDidMount() {
    this.props.getQuiz();
  }

  // Render application page
  render() {
    return (
      <div>
        <Navbar />
          {this.props.children}
        <Footer />
      </div>
    )
  }
}