import React from 'react'

export default class Footer extends React.Component {
	// Render footer Component
	render() {
		return (
			<footer className = 'footer'>
				<a>React, Redux, Bootstrap, MongoDB</a>
			</footer>
		);
	}
};