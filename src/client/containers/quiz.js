import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { getQuiz } from '../actions/quiz'

import Engine from '../components/engine'

// Connect to database load getQuiz to dispatch
@connect(
	state => ({
		quizArray: state.quiz,
	}),
	dispatch => ({
		getQuiz: bindActionCreators(getQuiz, dispatch)
	})
)
export default class Quiz extends React.Component {
	constructor() {
		super();
		this.state = {
			session: false,
			selectedQuiz: '',
			selectedQuizTitle: '',
			quiz: [],
			length: null
		}
		this.selectQuiz = this.selectQuiz.bind(this);
		this.startEngine = this.startEngine.bind(this);
		this.endEngine = this.endEngine.bind(this);
	}

	// Load get requisition prevent unload elements
	componentDidMount() {
		const { quizArray } = this.props;
		this.props.getQuiz();
		for (let i = 0; i < quizArray.length; i++) {
			if (quizArray[i]['title'] === 'Primeiro Quiz') {
				this.setState({
					selectedQuiz: quizArray[i]._id,
					selectedQuizTitle: quizArray[i].title,
					length: quizArray[i].questions.length
				});
			}
		}
	}

	// Handle the select quiz event
	selectQuiz(event) {
		const quizID = event.target.value;
		const { quizArray } = this.props;
		let { length } = this.state;
		let title = '';
		for (let i = 0; i < quizArray.length; i++) {
			if (quizArray[ i ]._id === quizID) {
				length = quizArray[ i ].questions.length;
				title = quizArray[ i ].title;
			}
		}
		this.setState({
			selectedQuiz: quizID,
			selectedQuizTitle: title,
			length
		});
	}

	// Function for Start Engine of solving questions
	startEngine() {
		const { selectedQuiz } = this.state;
		const quiz = this.props.quizArray.filter( (quiz) => quiz._id === selectedQuiz );
		this.setState({ quiz: quiz[0], session: true });
	}

	// Function for end the solving questions engine and save on leaderboards
	// Not usable Leaderboards not implemented
	endEngine(score) {
		this.setState({ session: false });
	}

	// Render the quizzes Selection from Quiz database
	render() {
		if (this.state.session) {
			return (
				<Engine
					endEngine = {this.endEngine}
					quiz = {this.state.quiz} />
			)
		} else {
			return (
				<div className = 'container'>
					<div className = 'col-md-5 form-area'>
						<h1>Select a Quiz to Engine</h1>
						<select
							className = 'selectpicker'
							onChange = {this.selectQuiz.bind(this)}
							value = {this.state.selectedQuiz}>
							if ( this.state.length === null){
								<option hidden defaultValue> Select The Quiz </option>
							} else
							{
								this.props.quizArray.map( (quiz, idx) => {
									return (
										<option	value = {quiz._id} key = {idx}>
											{quiz.title}
										</option>
									);
								})
							}
						</select>
						{
							this.state.length !== null &&
							<h2>
								This quiz has a total of {this.state.length} questions
							</h2>
						}
						{
							this.state.length !== null &&
							<button className = 'btn btn-primary' onClick = {this.startEngine}>
								Begin Quiz
							</button>
						}
					</div>
				</div>
			);
		}
	}
};