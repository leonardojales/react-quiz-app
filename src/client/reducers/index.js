import { combineReducers } from 'redux'

import quiz from './quiz-reducer'

// Combine Reducers
export default combineReducers({
  quiz
});
