
import { ADD_QUIZ, SAVE_QUIZ } from '../static/quiz'

// Define switch for each action from reducers
export default function quiz (state = [], action){

	switch(action.type) {

		case ADD_QUIZ:
			return state.concat(action.quiz);

		case SAVE_QUIZ:
			return [...action.data];

		default:
			return state;

	}

}