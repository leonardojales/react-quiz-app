import React from 'react'

export default class Engine extends React.Component {
	// Structure of questions engine
	constructor(props) {
		super(props);
		this.state = {
			index: 0,
			quiz: this.props.quiz,
			complete: false,
			answer: null,
			score: 0
		}
		this.nextQuestion = this.nextQuestion.bind(this);
		this.correctAnswer = this.correctAnswer.bind(this);
		this.wrongAnswer = this.wrongAnswer.bind(this);
	}
	// CorrectAnswer rule logic
	correctAnswer() { this.setState({ answer: true, score: this.state.score + 1 }) }
	// CorrectAnswer rule logic
	wrongAnswer() { this.setState({ answer: false }) }
	// nextQuest rule logic
	nextQuestion() {
		const length = this.props.quiz.questions.length;
		const { index } = this.state;
		if (index === length - 1) {
			this.setState({
				complete: true
			});
		} else {
			this.setState({
				index: this.state.index + 1,
				answer: null
			});
		}
	}

	// Render Question engine Component
	render() {
		const { index, quiz } = this.state;
		const currentQuestion = quiz.questions[index];
		const percentage = this.state.score / this.props.quiz.questions.length;
		return (
			<div className = 'container'>
				<div className = 'modal-content'>
					<div className = 'modal-header'>
						<h1 className = 'display-2'>{this.props.quiz.title}</h1>
					</div>
					<div className = 'modal-body'>
						{
							!this.state.complete &&
							<div>
								<h3 className = 'display-3'>Question {this.state.index + 1} of {quiz.questions.length}:</h3>
							</div>
						}
						{
							!this.state.complete &&
							<div className = 'modal-header'>
								<h1 className = 'display-1'>{currentQuestion.questionTitle}</h1>
							</div>
						}
						{
							!this.state.complete && currentQuestion.answers.map( (answer, idx) => {
								if (this.state.answer === null) {
									if (currentQuestion.correctAnswer === idx) {
										return (
											<label
												key = {idx}
												className = 'element-animation1 btn btn-lg btn-primary btn-block'
												onClick = {this.correctAnswer}>
												<p>{answer}</p>
											</label>	
										)
									} else {
										return (
											<label
												key = {idx}
												className = 'element-animation1 btn btn-lg btn-primary btn-block'
												onClick = {this.wrongAnswer}>
												<p>{answer}</p>
											</label>
										)
									}
								} else if (this.state.answer) {
									if (currentQuestion.correctAnswer === idx) {
										return (
											<label
												key = {idx}
												className = 'element-animation1 btn btn-lg btn-primary btn-block-correct opt-selected' id = 'correctWinner'>
												<p>{answer}</p>
											</label>	
										)
									} else {
										return (
											<label
												key = {idx}
												className = 'element-animation1 btn btn-lg btn-primary btn-block-incorrect' id = 'wrongWinner'>
												<p>{answer}</p>
											</label>
										)
									}
								} else {
									if (currentQuestion.correctAnswer === idx) {
										return (
											<label
												key = {idx}
												className = 'element-animation1 btn btn-lg btn-primary btn-block-correct' id = 'correctLoser'>
												<p>{answer}</p>
											</label>	
										)
									} else {
										return (
											<label
												key = {idx}
												className = 'element-animation1 btn btn-lg btn-primary btn-block-incorrect opt-selected' id = 'wrongLoser'>
												<p>{answer}</p>
											</label>
										)
									}				
								}
							})
						}

						{
							this.state.answer !== null && !this.state.complete &&
							<div>
								{ this.state.answer ? <h1>Correct Answer!</h1> : <h1>Wrong Answer!</h1> }
								{ this.state.index + 1 === quiz.questions.length ?
									<button className = 'btn btn-primary' onClick = {this.nextQuestion}>View Results</button>
									:
									<button className = 'btn btn-primary' onClick = {this.nextQuestion}>Next Question</button> }
							</div>
						}

						{
							this.state.complete &&
							<div>
								<h1>
									You scored {this.state.score} correct out of {this.props.quiz.questions.length} questions!
									<p>
										{ percentage > 0.75 ? ':) Good!' : ':( Keep Working!'}
									</p>
								</h1>
								<button className = 'btn btn-primary' onClick = {this.props.endEngine.bind(this, this.state.score / this.props.quiz.questions.length * 100 )}>
									Return to Quiz Page
								</button>
							</div>
						}
					</div>
				</div>
			</div>
		);
	}
};