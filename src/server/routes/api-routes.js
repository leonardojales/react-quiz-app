import express from 'express'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'

import Quiz from '../models/quiz.model'

const app = module.exports = express.Router();

// retrieve all quiz created from database
// and return to client
app.get('/get', (req, res) => {
	Quiz.find({}, (err, data) => {
		if( err )
			res.send(err);
		res.status(201).send(data);
	});
});

// Retrieve one quiz by id created from database
// and return to client
app.get('/get/:quiz_id', (req, res) => {
	Quiz.findById(req.params.quiz_id, function(err, data) {
		if( err )
			res.send(err);
		res.status(201).send(data);
	});
});

// post a new quiz from client to database
app.post('/save', (req, res) => {
	const quizData = req.body.quiz;
	// submit quiz to database

	const quiz = new Quiz({
		title: quizData.title,
		questions: quizData.questions
	});
	quiz.save( (err) => {
		if (err) throw err;
		res.status(201).send('success! saved!');
	});
});

// update quiz from client to database
app.put('/save/:quiz_id', (req, res) => {
	Quiz.findById(req.params.quiz_id, function(err, data) {
		if( err )
			res.send(err);
			const quizData = req.body.quiz;
			// submit quiz to database
			const quiz = new Quiz({
				title: quizData.title,
				questions: quizData.questions
			});
			quiz.save( (err) => {
				if (err) throw err;
				res.status(201).send('success! updated!');
			});
	});
});

// delete quiz from client to database
app.delete('/delete/:quiz_id', (req, res) => {
	Quiz.remove(req.params.quiz_id, function(err, data) {
		if( err )
			res.send(err);
		res.status(201).send('success! deleted!');
	});
});
