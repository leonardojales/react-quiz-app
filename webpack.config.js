var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

let devtool;
let entry;
let plugins;
let devServer;

devtool = 'cheap-module-eval-source-map';
devServer = {
  contentBase: './src/client',
};

module.exports = {
  devtool,
  devServer,

  entry: './src/client/index',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'src/client/')
  },
  module: {
    rules: [
        { 
            test: /\.js?$/, 
            loader: 'babel-loader', 
            exclude: /node_modules/ 
        },
        {
            test: /\.scss$/,
            use: [ 'style-loader', 'css-loader' ]
        }
    ]
  },
  node: {
    dns: "empty",
    fs: "empty",
    net: "empty",
    tls: "empty",
    module: "empty"
  }
};