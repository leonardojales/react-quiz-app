import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { Router, browserHistory } from 'react-router'
import { Provider } from 'react-redux'
import routes from './route'
import thunkMiddleware from 'redux-thunk'
import configureStore from './store/config'
import './style/index.scss'

export const store = configureStore();

// Render Application
render(
  <Provider store = {store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>,
  document.getElementById('App')
);