import mongoose from 'mongoose'

// Database Schema
const QuizSchema = mongoose.Schema({
	title: String,
	id: String,
	questions: Array
}, { collection: 'quiz'});

module.exports = mongoose.model('Quiz', QuizSchema);